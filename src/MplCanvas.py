import matplotlib  # type: ignore

from matplotlib.backends import backend_qt5agg  # type: ignore
from matplotlib.figure import Figure  # type: ignore

matplotlib.use('Qt5Agg')


class MplCanvas(backend_qt5agg.FigureCanvasQTAgg):

    def __init__(self):
        self.fig = Figure()
        self.axes = self.fig.add_subplot()
        super(MplCanvas, self).__init__(self.fig)

from sympy import solveset  # type: ignore
from sympy.parsing.sympy_parser import parse_expr  # type: ignore
import tokenize
import sympy  # type: ignore


def check_root(left: float, right: float, user_function: str,
               variable: str) -> bool:
    rightConditions = False
    var = sympy.Symbol(variable)
    root = solveset(user_function, var,
                    sympy.Interval(left, right)).__len__()
    if root == 1:
        rightConditions = True
    return rightConditions


def check_conditions(left: str, right: str, user_function: str,
                     variable: str) -> bool:
    rightConditions = True
    try:
        float(left)
        float(right)
        parse_expr(user_function).diff()
        parse_expr(user_function).diff(sympy.Symbol(variable))
    except(ValueError, SyntaxError, tokenize.TokenError):
        rightConditions = False
    if rightConditions:
        if float(left) >= float(right) or \
                parse_expr(user_function).diff(sympy.Symbol(variable)) == 0:
            rightConditions = False
    return rightConditions

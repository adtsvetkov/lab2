import unittest
from checking import check_conditions, check_root  # type: ignore


class TestForNewtonMethod(unittest.TestCase):
    def test_right(self):
        answer = check_conditions("-1", "1", "x**2", "x")
        assert answer is True

    def test_borders_conditions(self):
        answer = check_conditions("a", "b", "y**2", "y")
        assert answer is False

    def test_borders_values(self):
        answer = check_conditions("20", "-3", "y**2", "y")
        assert answer is False

    def test_random_function(self):
        answer = check_conditions("-3", "3", "sdlfsdlfjsdf", "y")
        assert answer is False

    def test_variable(self):
        answer = check_conditions("-3", "3", "x**2", "y")
        assert answer is False

    def test_wrong_function(self):
        answer = check_conditions("12", "15", "4", "x")
        assert answer is False

    def test_wrong_input(self):
        answer = check_conditions("12", "15", "x", "4")
        assert answer is False

    def test_one_root(self):
        answer = check_root(-2, 2, "x**2", "x")
        assert answer is True

    def test_many_roots(self):
        answer = check_root(-6, 6, "sin(x)", "x")
        assert answer is False

    def test_no_roots(self):
        answer = check_root(-2, 3, "x**2 + 1", "x")
        assert answer is False

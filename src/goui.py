import sympy  # type: ignore
import layout  # type: ignore
import numpy  # type: ignore

from PyQt5 import QtWidgets, QtCore  # type: ignore
from PyQt5.QtCore import QEventLoop  # type: ignore
from PyQt5.QtWidgets import QDialogButtonBox, QMessageBox  # type: ignore

from sympy.parsing.sympy_parser import parse_expr  # type: ignore
from matplotlib.colors import to_rgb  # type: ignore

from checking import check_conditions, check_root  # type: ignore


class NewtonApp(QtWidgets.QMainWindow, layout.Ui_Newton):

    def __init__(self):
        super(NewtonApp, self).__init__()
        self.timer = QtCore.QTimer()
        self.setupUi(self)

        grid_GroupBox = QtWidgets.QGridLayout(self.groupBox)
        grid_GroupBox.addWidget(self.Inserfunc, 0, 0)
        grid_GroupBox.addWidget(self.label, 0, 1)
        grid_GroupBox.addWidget(self.Inserta, 0, 2)
        grid_GroupBox.addWidget(self.Insertb, 0, 3)

        grid_GroupBox.addWidget(self.function, 1, 0)
        grid_GroupBox.addWidget(self.variable, 1, 1)
        grid_GroupBox.addWidget(self.a, 1, 2)
        grid_GroupBox.addWidget(self.b, 1, 3)

        grid_GroupBox.addWidget(self.FuncColor, 2, 0)
        grid_GroupBox.addWidget(self.TangColor, 2, 1)
        grid_GroupBox.addWidget(self.label_2, 2, 2, 1, 1)

        grid_GroupBox.addWidget(self.funcColor, 3, 0)
        grid_GroupBox.addWidget(self.tangentColor, 3, 1)
        grid_GroupBox.addWidget(self.comboBox, 3, 2)
        grid_GroupBox.addWidget(self.OK_NOK, 3, 3,
                                alignment=QtCore.Qt.AlignCenter)

        self.grid = QtWidgets.QGridLayout(self.centralwidget)
        self.grid.addWidget(self.graphicsView, 0, 1, 1, 3)
        self.grid.addWidget(self.groupBox, 1, 0, 1, 5)
        self.grid.setRowStretch(0, 1)
        self.grid.setRowStretch(1, 0)

        self.setTabOrder(self.function, self.variable)
        self.setTabOrder(self.variable, self.a)
        self.setTabOrder(self.a, self.b)
        self.setTabOrder(self.b, self.funcColor)
        self.setTabOrder(self.funcColor, self.tangentColor)
        self.setTabOrder(self.tangentColor, self.comboBox)
        self.setTabOrder(self.comboBox, self.OK_NOK)

        self.function.setFocus()

        self.OK_NOK.button(QDialogButtonBox.Ok).clicked.connect(self.ok_button)
        self.OK_NOK.button(QDialogButtonBox.Reset).clicked.connect(self.reset)

    def newton_method(self, a: float, b: float, function: str, x: str,
                      eps: float, func_col: str, tan_col: str) \
            -> float:

        # parsing function
        f = parse_expr(function)
        variable = sympy.Symbol(x)
        derivative = f.diff(variable)
        x_start = a + (b - a) / 2

        # drawing function
        h = (b - a) / 1000
        X = numpy.arange(a, b, h)
        Y = [f.subs(variable, x) for x in X]
        self.graphicsView.axes.cla()
        self.graphicsView.axes.set_title("Newton method for function "
                                         + function + " on [" + str(a)
                                         + ", " + str(b) + "]")
        self.graphicsView.axes.plot(X, Y, color=to_rgb(func_col))
        self.graphicsView.draw()
        self.show()

        while True:
            loop = QEventLoop()
            QtCore.QTimer.singleShot(1000, loop.quit)  # 2000 = 2 seconds
            loop.exec()
            self.graphicsView.axes.cla()
            self.graphicsView.axes.plot(X, Y, color=to_rgb(func_col))
            self.graphicsView.axes.plot(x_start,
                                        f.subs(variable, x_start), 'ko')
            # drawing tangent
            Y_tangent = [
                derivative.subs(variable, x_start) * x -
                derivative.subs(variable, x_start) * x_start
                + f.subs(variable, x_start) for x in X]
            self.graphicsView.axes.plot(X, Y_tangent,
                                        color=to_rgb(tan_col))
            self.graphicsView.axes.set_title("Newton method for function "
                                             + function + " on [" + str(a)
                                             + ", " + str(b) + "] \n"
                                             + "Now point is: "
                                             + "%.7f" % x_start)
            self.graphicsView.draw()
            self.show()
            if f.subs(variable, x_start) < eps:
                break
                # new point
            x_start = x_start - f.subs(variable,
                                       x_start) / derivative.subs(variable,
                                                                  x_start)
        return x_start

    def ok_button(self) -> None:

        flagError = False

        user_function = self.function.text()
        if not user_function:
            QMessageBox.about(self.centralWidget(),
                              "Message", "Enter your function, please")
            flagError = True
        variable = self.variable.text()
        if not variable:
            QMessageBox.about(self.centralWidget(),
                              "Message", "Enter your variable, please")
            flagError = True
        left = self.a.text()
        if not left:
            QMessageBox.about(self.centralWidget(),
                              "Message", "Enter your left border, please")
            flagError = True
        right = self.b.text()
        if not right:
            QMessageBox.about(self.centralWidget(),
                              "Message", "Enter your right border, please")
            flagError = True
        if flagError:
            return
        if not check_conditions(left, right, user_function, variable):
            QMessageBox.about(self.centralWidget(),
                              "Error",
                              "Please enter right parameters and function")
            self.reset()
            return
        if not check_root(float(left), float(right), user_function, variable):
            QMessageBox.about(self.centralWidget(),
                              "Error",
                              "There is no or more than one"
                              " root on selected interval")
            self.reset()
            return
        functionColor = self.funcColor.currentText()
        tangentColor = self.tangentColor.currentText()
        accuracy = self.comboBox.currentText()
        self.newton_method(float(left), float(right),
                           user_function, variable,
                           float(accuracy), functionColor,
                           tangentColor)

    def reset(self) -> None:
        self.function.clear()
        self.variable.clear()
        self.a.clear()
        self.b.clear()
        self.funcColor.setCurrentIndex(0)
        self.tangentColor.setCurrentIndex(0)
        self.comboBox.setCurrentIndex(0)
        return

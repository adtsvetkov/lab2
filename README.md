# Newton method visualization
## Purpose
Application shows root-finding Newton's algorithm step by step.
## Requirements
- python 3.8.x ([get at python.org](https://www.python.org/downloads/))
- additional packages (see in `requirements.txt`) 
## Installation
- Download these files from GitLab or clone this repository via https (don't forget to install git):
    ```
    git clone https://gitlab.com/adtsvetkov/lab2.git
    cd lab2
    ```
    If you want to run application from the `develop` branch:
    ```
    git checkout develop
- Install dependencies for command line application (use full paths if necessary):
   ```
   pip3 install -r requirements.txt
   ```
   If you use PyCharm, you can run this command from `lab2` project Terminal. Also don't forget to set `python 3.8.x` as project interpreter.
## Usage
- Run program from the root folder of repository: `python src/main.py` (use full paths if necessary)
  ```
  cd src
  python3 main.py
  ```
- You will see input-UI for your function to find root on suggested interval. Please enter your function and algorithm-parameters and press "OK". You will see visualization of newton algorithm for your input function step by step. 
 
## Explanation
UI-interface visualizes Newton's algorithm's work. You can choose colors for vizualisation and accuracy. First point is calculated as middle of right and left border. If you input wrong parameters, you will see message about it. For cleaning all values press "Reset".

You can run all unittests provided using `unittest.discover` (use full paths if necessary):
```
python3 -m unittest discover
```

## Example
First of all, UI-menu:  

<img src="https://sun9-34.userapi.com/X0uren2AGngUrUfyYUTDg3Bi6zyehj1PYhBKsw/4UGBZuPkh24.jpg"  width="639" height="592">

Enter your function:

<img src="https://sun9-72.userapi.com/ZSykH178F3z80eU-c7vmpuz_TJcancdtcO_TyQ/gihdjO4IFRk.jpg"  width="639" height="592">

And press "OK" (you'll see function graph first):

<img src="https://sun9-32.userapi.com/jx4zSHKBoPyN0pqD9M3DC9Mf9DIqyGypyx6S1A/B6xGspL8Mew.jpg"  width="639" height="592">

Next you will see step by step algorithm.  

_Step 1:_

<img src="https://sun9-2.userapi.com/VkiQvUoBA52lZg2FKndN6mp9JsrPV-D4NyJO1Q/jhKzAYCbyz8.jpg"  width="639" height="592">

_Step 2:_

<img src="https://sun9-74.userapi.com/R90C_nsXMUtARbh_96YCfPXqqK4_M_k9f-UR9A/zWaVU1wm6tw.jpg"  width="639" height="592">

_Step 3:_

<img src="https://sun9-63.userapi.com/cVF-c5DdNTTUBsolmCWKfwDWI4pMrmWZR-pA5A/icNaR5e-oUk.jpg"  width="639" height="592">

_Step 4:_

<img src="https://sun9-56.userapi.com/5kcSRMbhfPWI_IibjwsmE3w4YXQtiVdBeTbstg/UapZBzz3ElA.jpg"  width="639" height="592">

_Step 5:_

<img src="https://sun9-72.userapi.com/Kd3wooNtb1x-5AVQwjP1AwQP2WgAypV2lQ1VuA/VlVs7FALf3E.jpg"  width="639" height="592">

_Step 6:_

<img src="https://sun9-55.userapi.com/-dvfpa7vUKQAXM47h0v5UoN7NR-RdCEoTgqP1g/roLkjXpZ9HI.jpg"  width="639" height="592">
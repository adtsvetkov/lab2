import sys
from PyQt5 import QtWidgets  # type: ignore
from goui import NewtonApp  # type: ignore


def main() -> None:
    app = QtWidgets.QApplication(sys.argv)
    window = NewtonApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
